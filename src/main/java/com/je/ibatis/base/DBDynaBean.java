/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.ibatis.base;

import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 自定义动态类
 * <p>
 * 手工设定属性的内容，系统保留以下属性：
 * $TABLE_CODE$ :   表的名称
 * $ROWS$       :   结果记录集
 * $P_COUNT$      :   当前返回记录数
 * $A_COUNT$  :   总的查询记录数
 * $SQL$        :   查询的SQL语句
 * .....
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/12/11
 */
public class DBDynaBean implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = -2563215753689331432L;

    /**
     * 存放属性的值集
     */
    private HashMap<String, Object> values = new HashMap<String, Object>();

    public HashMap<String, Object> getValues() {
        return values;
    }

    public void setValues(HashMap<String, Object> values) {
        this.values = values;
    }

    /**
     * 设置值集
     *
     * @param valueMap 值集
     */
    public void setValues(Map<String, Object> valueMap) {
        this.values.clear();
        this.values.putAll(valueMap);
    }

    public DBDynaBean set(String keyPkCode, String pkCodeByTableCode) {
        put(keyPkCode, pkCodeByTableCode);
        return this;
    }

    /**
     * 存放值
     *
     * @param key   键
     * @param value 值
     * @return java.lang.Object
     */
    public Object put(String key, Object value) {
        //英文+数字+下划线
        String columnRegex = "^\\w+$";
        //校验是否匹配规则
        if (key != null && key.trim().matches(columnRegex)) {
            key = key.trim();
        }
        // 原逻辑
        if (!StringUtils.isEmpty(key)) {
            values.put(key, value);
        }
        return null;
    }

}
