/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.ibatis.scripting.xmltags;

import com.je.ibatis.session.CustomConfiguration;
import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * MetaHandler
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/11/20
 */
public class MetaHandler implements NodeHandler {

    private final CustomConfiguration configuration;

    protected MetaHandler(CustomConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void handleNode(XNode nodeToHandle, List<SqlNode> targetContents) {
        final String test = nodeToHandle.getStringAttribute("test");
        final String type = nodeToHandle.getStringAttribute("type");
        final String ignore = nodeToHandle.getStringAttribute("ignore");
        final String table = nodeToHandle.getStringAttribute("table");
        final String func = nodeToHandle.getStringAttribute("func");
        String alias = nodeToHandle.getStringAttribute("alias");
        if (!StringUtils.isEmpty(alias)) {
            alias = alias.trim();
            //是否无效 防止注入
            boolean invalid = alias.contains(" ") || alias.contains(".");
            if (invalid) {
                throw new RuntimeException("alias is invalid : " + alias);
            }
        }
        MetaSqlNode metaSqlNode = new MetaSqlNode(configuration, test, type, ignore, table, func, alias);
        targetContents.add(metaSqlNode);
    }
}