/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.ibatis.extension.keygen;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.executor.keygen.KeyGenerator;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.util.StringUtils;

/**
 * MetaKeyGenerator
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/11/22
 */
public abstract class BaseMetaKeyGenerator implements KeyGenerator {

    /**
     * 日志
     */
    protected final Log log = LogFactory.getLog(this.getClass());
    /**
     * 表名
     */
    protected final String tableCode;

    /**
     * 主键列
     */
    protected final String keyColumns;

    public BaseMetaKeyGenerator(String tableCode, String keyColumns) {
        this.tableCode = tableCode;
        this.keyColumns = keyColumns;
    }

    public String getTableCode() {
        return tableCode;
    }

    public String[] getKeyColumnNames() {
        return keyColumns.split(",");
    }

    public String[] getKeyProperties() {
        return keyColumns.split(",");
    }

    public Boolean pkValueIsNull(MetaObject metaParam) {
        if (metaParam.hasGetter(keyColumns)) {
            //元数据包含ID 则不自动生成
            Object idValue = metaParam.getValue(keyColumns);
            if (idValue != null && !StringUtils.isEmpty(idValue.toString())) {
                log.info("insert metadata has id : " + idValue);
                return false;
            }
        }
        return true;
    }
}