/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.ibatis.extension.metadata;

/**
 * IdType
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/11/22
 */
public enum IdType {
    /**
     * 自增
     */
    AUTO("AUTO"),
    /**
     * 序列
     */
    AUTO_INCREMENTER("AUTO_INCREMENTER"),
    /**
     * 自定义sql
     */
    SQL("SQL"),
    /**
     * 不含_的uuid
     */
    UUID("UUID"),
    /**
     * 无状态
     */
    NONE("NONE");

    private String value;

    IdType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static IdType getIdType(Object type) {
        return type == null ? UUID : getIdType(type.toString());
    }

    public static IdType getIdType(String type) {
        if(type==null){
            return UUID;
        }
        if (type.equals(AUTO.getValue())) {
            return AUTO;
        }
        if (type.equals(AUTO_INCREMENTER.getValue())) {
            return AUTO_INCREMENTER;
        }
        if (type.equals(SQL.getValue())) {
            return SQL;
        }
        if (type.equals(UUID.getValue())) {
            return UUID;
        }
        if (type.equals(NONE.getValue())) {
            return NONE;
        }
        return UUID;
    }

}
