/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.ibatis.extension.metadata.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Table
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/11/21
 */
public class Table extends BaseMeta {

    /**
     * 表名
     */
    private String code;
    /**
     * 表注释
     */
    private String name;
    /**
     * 主键
     */
    private Id id;
    /**
     * 表列
     */
    private Map<String, Column> columns;

    public Table() {
        this.columns = new ConcurrentHashMap<>();
    }

    public Table(String code) {
        this();
        this.code = code;
    }

    public Table(String code, String name, Id id, Map<String, Column> columns) {
        this.code = code;
        this.name = name;
        this.id = id;
        this.columns = columns;
    }

    /**
     * 添加列
     *
     * @param column 列对象
     * @return com.je.ibatis.extension.metadata.model.Table
     */
    public Table addColumn(Column column) {
        this.columns.put(column.getCode(), column);
        return this;
    }

    public Table addColumn(String columnCode) {
        return addColumn(new Column(columnCode));
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public Map<String, Column> getColumns() {
        return columns;
    }

    public void setColumns(Map<String, Column> columns) {
        this.columns = columns;
    }

    public List<Column> getColumnList() {
        return new ArrayList<>(getColumns().values());
    }

    @Override
    public String toString() {
        return "Table{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", id=" + id +
                ", columns=" + columns +
                '}';
    }
}