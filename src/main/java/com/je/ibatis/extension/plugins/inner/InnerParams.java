/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.ibatis.extension.plugins.inner;

import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.sql.Connection;

/**
 * 插件内部参数
 */
public class InnerParams {

    private Object target;
    private MappedStatement mappedStatement;
    private Object parameter;
    private RowBounds rowBounds;
    private ResultHandler resultHandler;
    private CacheKey cacheKey;
    private BoundSql boundSql;

    public Object getTarget() {
        return target;
    }

    public MappedStatement getMappedStatement() {
        return mappedStatement;
    }

    public Object getParameter() {
        return parameter;
    }

    public RowBounds getRowBounds() {
        return rowBounds;
    }

    public ResultHandler getResultHandler() {
        return resultHandler;
    }

    public CacheKey getCacheKey() {
        return cacheKey;
    }

    public BoundSql getBoundSql() {
        return boundSql;
    }

    public InnerParams target(Object target) {
        this.target = target;
        return this;
    }

    public InnerParams mappedStatement(MappedStatement mappedStatement) {
        this.mappedStatement = mappedStatement;
        return this;
    }

    public InnerParams parameter(Object parameter) {
        this.parameter = parameter;
        return this;
    }

    public InnerParams rowBounds(RowBounds rowBounds) {
        this.rowBounds = rowBounds;
        return this;
    }

    public InnerParams resultHandler(ResultHandler resultHandler) {
        this.resultHandler = resultHandler;
        return this;
    }

    public InnerParams cacheKey(CacheKey cacheKey){
        this.cacheKey = cacheKey;
        return this;
    }

    public InnerParams boundSql(BoundSql boundSql) {
        this.boundSql = boundSql;
        return this;
    }

    public static InnerParams build() {
        return new InnerParams();
    }

}
