/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.ibatis.extension.plugins.pagination;

import com.je.ibatis.extension.enums.DbType;
import com.je.ibatis.extension.plugins.pagination.dialects.*;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 方言工厂
 *
 * @FileName DialectFactory.java
 * @Author WangMengmeng E-mail:wangmeng6447@163.com
 * @CreatTime 2016年10月23日 下午6:48:41
 * @Version 1.0
 */
public class DialectFactory {


    /**
     * 方言缓存
     */
    private static final Map<String, IDialect> DIALECT_CACHE = new ConcurrentHashMap<>();

    /**
     * 获取数据库方言
     *
     * @param jdbcUrl 数据库连接地址
     * @return 分页语句组装类
     */
    public static IDialect getDialectByUrl(String jdbcUrl) {
        return getDialect(DbType.getDbTypeByUrl(jdbcUrl));
    }

    /**
     * 获取数据库方言
     *
     * @param dbType 数据库类型
     * @return 分页语句组装类
     */
    public static IDialect getDialect(String dbType) {
        return getDialect(DbType.getDbType(dbType));
    }

    /**
     * 获取数据库方言
     *
     * @param dbType 数据库类型
     * @return 分页语句组装类
     */
    private static IDialect getDialect(DbType dbType) {
        IDialect dialect = DIALECT_CACHE.get(dbType.getDb());
        if (null == dialect) {
            // 缓存方言
            dialect = getDialectByDbType(dbType);
            DIALECT_CACHE.put(dbType.getDb(), dialect);
        }
        return dialect;
    }

    /**
     * 根据数据库类型选择不同分页方言
     *
     * @param dbType 数据库类型
     * @return 分页语句组装类
     */
    private static IDialect getDialectByDbType(DbType dbType) {
        switch (dbType) {
            case MYSQL:
                return new MySqlDialect();
            case MARIADB:
                return new MariaDBDialect();
            case ORACLE:
                return new OracleDialect();
            case DB2:
                return new DB2Dialect();
            case H2:
                return new H2Dialect();
            case SQL_SERVER:
                return new SQLServerDialect();
            case SQL_SERVER2005:
                return new SQLServer2005Dialect();
            case POSTGRE_SQL:
                return new PostgreDialect();
            case HSQL:
                return new HSQLDialect();
            case SQLITE:
                return new SQLiteDialect();
            case DM:
                return new DmDialect();
            case OSCAR:
                return new OscarDialect();
            case KINGBASEES:
                return new KingbaseESDialect();
            default:
                throw new RuntimeException("The Database's IDialect Not Supported!");
        }
    }

}
