/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.ibatis.extension.toolkit;

import org.apache.ibatis.executor.ExecutorException;
import org.apache.ibatis.reflection.MetaObject;

/**
 * Mapper方法参数处理
 * <p>
 * 1.根据参数解析tableCode
 * 2.参数中设置主键策略生成的主键值
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/11/25
 */
public class ParameterUtil {

    public static String tableCode(MetaObject metaParam) {
        if (metaParam.hasGetter(Constants.KEY_TABLE_CODE) && metaParam.getValue(Constants.KEY_TABLE_CODE) != null) {
            return metaParam.getValue(Constants.KEY_TABLE_CODE).toString();
        }
        if (metaParam.hasGetter(Constants.KEY_WRAPPER_TABLE_CODE) && metaParam.getValue(Constants.KEY_WRAPPER_TABLE_CODE) != null) {
            return metaParam.getValue(Constants.KEY_WRAPPER_TABLE_CODE).toString();
        }
        if (metaParam.hasGetter(Constants.KEY_MAP_TABLE_CODE) && metaParam.getValue(Constants.KEY_MAP_TABLE_CODE) != null) {
            return metaParam.getValue(Constants.KEY_MAP_TABLE_CODE).toString();
        }
        return null;
    }

    public static String functionCode(MetaObject metaParam) {
        if (metaParam.hasGetter(Constants.KEY_FUNCTION_CODE) && metaParam.getValue(Constants.KEY_FUNCTION_CODE) != null) {
            return metaParam.getValue(Constants.KEY_FUNCTION_CODE).toString();
        }
        if (metaParam.hasGetter(Constants.KEY_WRAPPER_FUNCTION_CODE) && metaParam.getValue(Constants.KEY_WRAPPER_FUNCTION_CODE) != null) {
            return metaParam.getValue(Constants.KEY_WRAPPER_FUNCTION_CODE).toString();
        }
        return null;
    }

    public static String selectColumns(MetaObject metaParam) {
        if (metaParam.hasGetter(Constants.KEY_SELECT_COLUMN) && metaParam.getValue(Constants.KEY_SELECT_COLUMN) != null) {
            return metaParam.getValue(Constants.KEY_SELECT_COLUMN).toString();
        }
        if (metaParam.hasGetter(Constants.KEY_WRAPPER_SELECT_COLUMN) && metaParam.getValue(Constants.KEY_WRAPPER_SELECT_COLUMN) != null) {
            return metaParam.getValue(Constants.KEY_WRAPPER_SELECT_COLUMN).toString();
        }
        return null;
    }

    public static void setValue(MetaObject metaParam, String property, Object value) {
        if (metaParam.hasSetter(property)) {
            metaParam.setValue(property, value);
        } else {
            throw new ExecutorException("No setter found for the keyProperty '" + property + "' in " + metaParam.getOriginalObject().getClass().getName() + ".");
        }
    }
}