# 包结构及关键代码讲解

### SqlSessionFactoryBean
启动时的入口，核心修改在`buildSqlSessionFactory`方法中，相关修改有注释，使用自定义的 Custom 相关类。

### CustomConfiguration
继承自mybatis的`Configuration`，修改了一些默认配置，增加了一些成员变量，有注释。

### binding、builder
&emsp;&emsp;这两个包的类是在原类名前加上Custom，代码中引用了其他相关的Custom类。主要的目的是在dtd中增加`<meta>`标签约束，保证`<meta>`通过xml解析器，
成功生成`MappedStatement`。使用到的Custom类会在类注释上标明。

&emsp;&emsp;CustomXMLStatementBuilder类中修改了`parseStatementNode`方法，增加了对 `generator=meta`的支持
（在`MetaBaseMapper.xml`的`insertBatch`、`insertMap`方法中有用到）。
主要作用是支持我们自定义的元数据路由主键策略`com.je.ibatis.extension.keygen.MetaRouterKeyGenerator`，
元数据路由主键策略主要根据参数中的tableCode去`CustomConfiguration.keyGenerators`找到资源表对应的主键策略。

### executor.statement
&emsp;&emsp;主要作用是重写`PreparedStatementHandler`使`MetaRouterKeyGenerator`策略生效，主要修改了`instantiateStatement(Connection connection)`方法。
通过`MetaRouterKeyGenerator.routerKeyGenerator`方法获取资源表真实主键策略。

### scripting.xmltags
&emsp;&emsp;增加`MetaHandler`、`MetaSqlNode`、`NodeHandler`元数据标签类，

###### CustomXMLScriptBuilder
重写`XMLScriptBuilder`并使用新增的元数据标签类，实现`<meta>`标签到`MetaSqlNode`对象的转换

###### CustomXMLLanguageDriver
自定义动态语言加载器，重写`XMLLanguageDriver`并使用`CustomXMLScriptBuilder`,在`CustomConfiguration`构造函数中指定为默认LanguageDriver。

###### CustomXMLScriptBuilder
重写`XMLScriptBuilder`并使用新增的元数据标签类，实现`<meta>`标签到`MetaSqlNode`对象的转换

### CustomDynamicSqlSource！！！！重要
&emsp;&emsp;继承并重写`DynamicSqlSource`中的`getBoundSql`方法，在`CustomXMLLanguageDriver.createSqlSource`方法中创建。

###### getBoundSql
获取方法调用的传参，判断是否包含`ConditionsWrapper`对象，如果包含则调用`ConditionsWrapper.getSqlNode().apply(context)`将条件解析器的whereSql
追加到`DynamicContext`内容中。支持打印sql语句，通过配置文件中`sqlParamLog=true`控制。

## extension 自定义代码拓展包
&emsp;&emsp;包括元数据缓存、自定义主键策略、条件解析器、分页插件、通用Mapper几部分。

### 元数据缓存
主要包括 builder、cache、metadata、parse 相关包。

##### metadata
资源表、功能信息相关的POJO。

##### parse
元数据解析器，读取数据库中的数据，生成POJO。默认使用`DefaultMetaDataParse`，自定义解析器需要实现`MetaDataParse`接口并修改mybatis配置。

##### cache
缓存管理器，缓存metadata相关的POJO。默认使用 `DefaultMetaDataCacheManager`，自定义缓存管理器需要实现`MetaDataCacheManager`接口并修改mybatis配置。

##### builder
元数据构建器 `MetaStatementBuilder` 包含元数据解析器、缓存管理器实例， 包含以下方法：
- builderFunction、function: 获取功能对象
- builderTable、table: 获取表对象
- keyGen：读取表主键信息 生成对应类型的主键策略并放入`CustomConfiguration.keyGenerators`
- getCacheManager: 获取元数据缓存管理器
- getParse: 获取元数据解析器
- getConfiguration: 获取MyBatis全文配置对象
- builder: 加载通用Mapper文件

&emsp;&emsp;项目启动时通过 `SqlSessionFactoryBean.buildSqlSessionFactory` 方法中调用`MetaStatementBuilder.builder()`
加载通用Mapper文件`com/je/ibatis/extension/mapper/MetaBaseMapper.xml`

&emsp;&emsp;在`MetaSqlNode`中通过`configuration.getMetaStatementBuilder()`获取Builder对象，通过Builder对象获取功能数据、资源表数据。

### 自定义主键策略

主要包括 MetaRouterKeyGenerator（路由）、MetaUUIDKeyGenerator（UUID）、MetaAutoKeyGenerator（自动）、MetaSelectKeyGenerator（查询）四种策略。

 - MetaRouterKeyGenerator：主要用在通用Mapper的初始化时，设定主键为路由策略。当真正执行插入方法时会返回以下三种具体的主键策略。
 - MetaUUIDKeyGenerator：UUID生成策略。主要是在执行插入前读取到表的主键key，像map中插入UUID。目前全部使用UUID
 - MetaAutoKeyGenerator：数据库自增策略。插入执行完毕后从结果中读取主键值，放入插入参数的map中。目前未使用
 - MetaSelectKeyGenerator：查询主键策略。执行一段sql使用返回结果作为key。目前未使用

KeySqlGenerator 及其实现类。主要帮助 MetaSelectKeyGenerator 策略根据不同数据库类型生成查询SQL语句。

### 条件解析器
ConditionsWrapper 使用方法参见readme文档，主要包括以下几个主要属性。

- StringBuffer buffer：缓冲区。每次flush都会将缓冲区内容生成 SqlNode 放入 contents，同时将内容追加到 sqlContent。然后清空缓冲区。
- StringBuffer sqlContent：条件解析器中的全量SQL。
- List<SqlNode> contents：全量SQL的SqlNode版本。内容等同 sqlContent ，主要在 `CustomDynamicSqlSource.getBoundSql` 追加sql使用。
- ParameterWrapper parameter：条件解析器中的所有参数。继承HashMap
    - key默认规则为`String key = "wrapper" + super.size();`
    - 特殊key有`$TABLE_CODE$`资源表编码 `$FUNCTION_CODE$`功能编码

### 分页插件

代码在 plugins 包中，使用 `PaginationInterceptor` 拦截器实现，`plugins.pagination.dialects`中实现了各种数据库的分页方言。
实现方式是在执行查询时，参数发现分页对象，则获取当前语句查询总数，计算分页参数并使用分页方言包装当前语句进行查询。

### 通用Mapper
包括`MetaBaseMapper`接口和`MetaBaseMapper.xml`文件，

- MetaBaseMapper：平台通用方法。具体可看注释。
- MetaBaseMapper.xml：通用方法对应的xml解析。需要注意以下方法
    - insertBatch、insertMap：批量插入和插入。需要启用元数据路由主键策略`useGeneratedKeys="true" generator="meta"`
    - selectOneByPk：使用静态语句加动态标签的形式
    - selectSql、insertSql、updateSql、deleteSql：xml中都使用了if标签，添加原因是只有包含动态标签，才会使用`CustomDynamicSqlSource`


